# Introduzione al Deep Learning

Questo progetto contiene gli appunti in LaTex e le applicazioni pratiche in Python che ho appreso dal corso di Deep Learning [Deep Learning A-Z™: Hands-On Artificial Neural Networks](https://www.udemy.com/course/deeplearning/).

### Prerequisiti

- Livello di matematica da scuola superiore
- Conoscenze basilari di programmazione
- Consigliato ma non fondamentale: aver già visto letto [Fondamenti di ML](https://gitlab.com/marco.cordoni/fondamenti-di-ml)

## Autore

  - **Marco Cordoni**
